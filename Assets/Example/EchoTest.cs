﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class EchoTest : MonoBehaviour {

	public Text echo;

	// Use this for initialization
	IEnumerator Start () {
		echo = GameObject.Find("echo").GetComponent<Text> ();

		WebSocket w = new WebSocket(new Uri("ws://echo.websocket.org"));
		yield return StartCoroutine(w.Connect());

		w.SendString("Hi there");
		int i=0;
		while (true)
		{
			string reply = w.RecvString();
			if (reply != null)
			{
				Debug.Log ("Received: "+reply);
				echo.text = reply;
				w.SendString("Hi there"+i++);
			}
			if (w.error != null)
			{
				Debug.LogError ("Error: "+w.error);
				break;
			}
			yield return 0;
		}
		w.Close();
	}
}
